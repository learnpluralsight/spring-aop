package com.pluralsight.springaop.flightsmanagement.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;

import java.util.logging.Logger;

@Aspect
@Order(1)
public class LogginAspect1 {

    private final Logger logger =
            Logger.getLogger(LogginAspect1.class.getName());

    @Before("execution(public String com.pluralsight.springaop.flightsmanagement.domain.Flight.getId())")
    public void logginAdviceGetId(){
        logger.info("Flight getId method will be called");
    }

    @AfterReturning("execution(public * *.print())")
    public void logginAdvicePrint(){
        logger.warning("A print method has been called");
    }

    @Pointcut("within(com.pluralsight.springaop.flightsmanagement.domain.Ticket)")
    public void allTicketMethods(){
        //Pointcut aspect
    }

    @After("allTicketMethods()")
    public void logginAdvice(JoinPoint joinPoint){
        logger.info("A ticket method had been called");
        logger.info(joinPoint.toString());
        logger.info(joinPoint.getTarget().toString());
    }
}

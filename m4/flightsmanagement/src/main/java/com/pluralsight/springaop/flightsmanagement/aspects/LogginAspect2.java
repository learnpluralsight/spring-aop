package com.pluralsight.springaop.flightsmanagement.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;

import java.util.logging.Logger;

@Aspect
@Order(2)
public class LogginAspect2 {
    private final Logger logger =
            Logger.getLogger(LogginAspect2.class.getName());

    @Pointcut("execution(* com.pluralsight.springaop.flightsmanagement.domain.*.*set*(..))")
    public void allSetters(){
        //Pointcut AOP
    }

    @Around("allSetters()")
    public Object log(ProceedingJoinPoint thisJoinPoint) throws Throwable{
        String methodName = thisJoinPoint.getSignature().getName();

        Object[] methodArgs = thisJoinPoint.getArgs();

        logger.severe("Call method " + methodName
                + " with args " + methodArgs[0]);

        Object result = thisJoinPoint.proceed();

        logger.severe("Method " + methodName
                + " result " + result);

        return result;
    }
}
